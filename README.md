# Software Barbearia

## Nome Da Equipe
Bruno Piva Do Canto e Leonardo Larzen Viana

## Turma
353

## Objetivo Geral
Desenvolver um software para ajudar a gerenciar e fazer divulgação  da  barbearia.

## Objetivo Especifico
O site será feito para facilitar o acesso do cliente às informações de cortes, produtos e preços da barbearia.
    * Facilitar o acesso do cliente à informações.
    * Divulgação da barbearia.
    * Facilitar o agendamento do serviço.
