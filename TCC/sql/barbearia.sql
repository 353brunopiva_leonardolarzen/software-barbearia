-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25-Set-2019 às 16:44
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `barbearia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendamento`
--

CREATE TABLE IF NOT EXISTS `agendamento` (
  `id_agendamento` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_corte` int(11) NOT NULL,
  `observacao` text NOT NULL,
  `data` date NOT NULL,
  `horario` time NOT NULL,
  PRIMARY KEY (`id_agendamento`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `agendamento`
--

INSERT INTO `agendamento` (`id_agendamento`, `id_usuario`, `id_corte`, `observacao`, `data`, `horario`) VALUES
(20, 3, 1, 'teste', '2019-10-05', '02:57:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cortes`
--

CREATE TABLE IF NOT EXISTS `cortes` (
  `id_corte` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` text NOT NULL,
  `valor` double NOT NULL,
  `imagem` text NOT NULL,
  PRIMARY KEY (`id_corte`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `cortes`
--

INSERT INTO `cortes` (`id_corte`, `nome`, `descricao`, `valor`, `imagem`) VALUES
(1, 'Degrade na 1 sem risco', 'Corte bonitão', 15, 'img/degrade0.jpg'),
(2, 'Degrade na 1 sem risco', 'corte modinha', 20, 'img/degrade1.jpg'),
(3, ' Degrade na 2 ', 'corte estilo', 20, 'img/degrade2.jpg'),
(4, ' Degrade na 3', 'corte estiloso', 20, 'img/degrade3.jpg'),
(5, ' Degrade na 0 com Risco simples', 'corte massa', 20, 'img/degraderiscoso.jpg'),
(6, 'Degrade com risco na sombrancelha', 'corte famoso', 25, 'degraderiscocomp.jpg'),
(7, 'Corte Rústico', 'corte antigo', 25, 'corteest.jpg'),
(8, 'Degrade na 0 com Desenho', 'corte na 0', 30, 'degraderiscoso.jpg'),
(9, 'Degrade mais platinad', 'corte top', 95, 'platinado.jpg'),
(10, 'Degrade com Risco Platinado', 'corte massa', 100, 'plat1.jpg'),
(11, 'Degrade, Platinado com Desenho', 'corte desenhadao', 120, 'plat2.jpg'),
(12, 'Degrade blindado', 'corte maiss top', 70, 'blin.jpg'),
(13, 'Degrade Blindado com Desenho', 'corte blinadado raiz', 80, 'blindes.jpg'),
(14, 'Degrade Blindado e Platinado', 'corte mais chave', 150, 'blindesc.jpg'),
(15, 'Degrade Descolorido', 'corte de vila', 50, 'desc.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tcc_logincad`
--

CREATE TABLE IF NOT EXISTS `tcc_logincad` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(100) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `telefone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nome`, `telefone`, `email`, `senha`) VALUES
(1, 'Ramon', '12313', 'ramon@gmail.com', '1234'),
(2, 'br', '99', 'br@gmail.com', '1234'),
(3, 'Leo', '(45) 612156456', 'hsgdush@hotmail.com', '123'),
(4, 'guilhermao mangillao', '98322499', 'gui.marque@outlook.com', '1234'),
(5, 'ana', '4524456', 'vas@vas', '12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
